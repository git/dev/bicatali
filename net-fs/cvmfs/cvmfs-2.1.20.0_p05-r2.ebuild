# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit cmake-utils linux-info

MYP=${PN}-${PV/_p/-fix}

DESCRIPTION="HTTP read-only file system for distributing software"
HOMEPAGE="http://cernvm.cern.ch/portal/filesystem"
SRC_URI="https://github.com/cvmfs/${PN}/archive/${MYP}.tar.gz"

LICENSE="BSD"
SLOT="0"

KEYWORDS="~amd64 ~x86"
IUSE="aufs debug doc server test test-programs"

CDEPEND="
	dev-cpp/gtest
	dev-db/sqlite:3=
	dev-libs/openssl:0
	net-libs/pacparser:0=
	net-misc/curl:0=[adns]
	sys-apps/attr
	sys-libs/zlib:0=
	>=dev-cpp/sparsehash-1.12
	dev-libs/leveldb:0=
	sys-fs/fuse:0=
	server? (
		>=dev-python/geoip-python-1.3.1
		>=dev-cpp/tbb-4.3:0= )"

RDEPEND="${CDEPEND}
	app-admin/sudo
	net-fs/autofs
	server? (
		aufs? ( || (
			sys-fs/aufs3
			sys-fs/aufs4
			sys-kernel/aufs-sources ) )
		www-servers/apache )"

DEPEND="${CDEPEND}
	virtual/pkgconfig
	doc? ( app-doc/doxygen[dot] )
	test? (
		>=dev-python/geoip-python-1.3.1
		>=dev-cpp/tbb-4.3:0= )"

REQUIRED_USE="test-programs? ( server )"

S="${WORKDIR}/${PN}-${MYP}"

pkg_setup() {
	if use server; then
		if use aufs; then
			CONFIG_CHECK="~AUFS_FS"
			ERROR_AUFS_FS="CONFIG_AUFS_FS: is required to be set with the aufs flag"
		else
			CONFIG_CHECK="~OVERLAY_FS"
			ERROR_AUFS_FS="CONFIG_OVERLAY_FS: is required to be set"
		fi
		linux-info_pkg_setup
	fi
}

PATCHES=(
	"${FILESDIR}"/${P}-geoip-missing-cmake.patch
	"${FILESDIR}"/${P}-cares-underlink.patch
	"${FILESDIR}"/${P}-overlayfs.patch
)

src_prepare() {
	cmake-utils_src_prepare

	# gentoo stuff
	sed -i -e 's/COPYING//' CMakeLists.txt || die
	rm bootstrap.sh || die
	sed -i \
		-e "s:cvmfs-\${CernVM-FS_VERSION_STRING}:${PF}:" \
		CMakeLists.txt || die

	# should be gone in >= 2.2
	sed -i \
		-e 's|/etc/auto.master|/etc/autofs/auto.master|g' \
		-e 's|/etc/sysconfig/autofs|/etc/conf.d/autofs|g' \
		cvmfs/cvmfs_config || die

	sed -i \
		-e 's|${res_path}/conf.d|${res_path/modules.d|' \
		cvmfs/cvmfs_server || die

	# hack for bundled vjson
	# vjson not worth unbundling, already upstream obsolete
	# upstream replaced by gason with a new api
	if use server; then
		sed -i \
			-e 's/g++/$(CXX)/g' \
			-e 's/-O2/$(CXXFLAGS)/g' \
			-e 's/ar/$(AR)/' \
			-e 's/ranlib/$(RANLIB)/' \
			externals/vjson/src/Makefile || die
		mkdir -p "${WORKDIR}/${P}_build"/externals/build_vjson
		cp externals/vjson/src/* \
		   "${WORKDIR}/${P}_build"/externals/build_vjson/ || die
	fi
}

src_configure() {
	local mycmakeargs=(
		-DGEOIP_BUILTIN=OFF
		-DGOOGLETEST_BUILTIN=OFF
		-DLEVELDB_BUILTIN=OFF
		-DLIBCURL_BUILTIN=OFF
		-DPACPARSER_BUILTIN=OFF
		-DSPARSEHASH_BUILTIN=OFF
		-DSQLITE3_BUILTIN=OFF
		-DTBB_PRIVATE_LIB=OFF
		-DZLIB_BUILTIN=OFF
		-DBUILD_CVMFS=ON
		-DBUILD_LIBCVMFS=ON
		-DINSTALL_MOUNT_SCRIPTS=ON
		$(cmake-utils_use server BUILD_SERVER)
	)
	if use test || use test-programs; then
		mycmakeargs+=( -DBUILD_UNITTESTS=ON )
	fi
	use test-programs && mycmakeargs+=( -DINSTALL_UNITTESTS=ON )
	if use debug; then
		mycmakeargs+=(
			$(cmake-utils_use server BUILD_SERVER_DEBUG)
			$(cmake-utils_use test BUILD_UNITTESTS_DEBUG)
		)
	fi
	cmake-utils_src_configure
}

src_compile() {
	cmake-utils_src_compile
	if use doc; then
		cd doc
		doxygen cvmfs.doxy || die
	fi
}

src_install() {
	cmake-utils_src_install
	use doc && dohtml -r doc/html/*
}

pkg_config() {
	einfo "Setting up CernVM-FS client"
	cvmfs_config setup
	einfo "Now edit ${EROOT%/}/etc/cvmfs/default.local"
	einfo "and restart the autofs service"
}
