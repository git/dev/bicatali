# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

EGIT_REPO_URI="https://github.com/cvmfs/cvmfs.git"
EGIT_BRANCH="devel"

inherit cmake-utils linux-info git-r3 bash-completion-r1

SRC_URI=""
KEYWORDS=""

DESCRIPTION="HTTP read-only file system for distributing software"
HOMEPAGE="http://cernvm.cern.ch/portal/filesystem"

LICENSE="BSD"
SLOT="0"

IUSE="aufs debug doc server test test-programs"

CDEPEND="
	dev-cpp/gtest
	dev-db/sqlite:3=
	dev-libs/openssl:0
	net-libs/pacparser:0=
	net-misc/curl:0=[adns]
	sys-apps/attr
	sys-libs/zlib:0=
	>=dev-cpp/sparsehash-1.12
	dev-libs/leveldb:0=
	sys-fs/fuse:0=
	server? (
		>=dev-python/geoip-python-1.3.1
		>=dev-cpp/tbb-4.3:0= )"

RDEPEND="${CDEPEND}
	app-admin/sudo
	net-fs/autofs
	server? (
		aufs? ( || (
			sys-fs/aufs3
			sys-fs/aufs4
			sys-kernel/aufs-sources ) )
		www-servers/apache )"

DEPEND="${CDEPEND}
	virtual/pkgconfig
	doc? ( app-doc/doxygen[dot] )
	test? (
		>=dev-python/geoip-python-1.3.1
		>=dev-cpp/tbb-4.3:0= )"

PDEPEND="test-programs? ( sys-devel/gdb )"

REQUIRED_USE="test-programs? ( server )"

pkg_setup() {
	if use server; then
		if use aufs; then
			CONFIG_CHECK="~AUFS_FS"
			ERROR_AUFS_FS="CONFIG_AUFS_FS: is required to be set with the aufs flag"
		else
			CONFIG_CHECK="~OVERLAY_FS"
			ERROR_AUFS_FS="CONFIG_OVERLAY_FS: is required to be set"
		fi
		linux-info_pkg_setup
	fi
}

src_prepare() {
	cmake-utils_src_prepare

	# this removal avoids all bundled packages to be configured and built
	rm bootstrap.sh || die

	# specific gentoo paths and qa fixes
	sed -i -e 's/COPYING//' CMakeLists.txt || die
	sed -i \
		-e "s:cvmfs-\${CernVM-FS_VERSION_STRING}:${PF}:" \
		CMakeLists.txt || die
	sed -i \
		-e "s:/etc/bash_completion.d:$(get_bashcompdir):" \
		cvmfs/CMakeLists.txt || die

	# hack for bundled packages
	# not worth unbundling upstreams are flaky/dead
	local pkg
	for pkg in vjson sha2 sha3; do
		# respect toolchain variables
		sed -i \
			-e 's/g++/$(CXX)/g' \
			-e 's/gcc/$(CC)/g' \
			-e 's/CFLAGS/MYCFLAGS/g' \
			-e 's/-O2/$(CFLAGS)/g' \
			-e 's/-O2/$(CXXFLAGS)/g' \
			-e 's/ar/$(AR)/' \
			-e 's/ranlib/$(RANLIB)/' \
			externals/${pkg}/src/Makefile || die
		mkdir -p "${WORKDIR}/${P}_build"/externals/build_${pkg}
		cp -r externals/${pkg}/src/* \
		   "${WORKDIR}/${P}_build"/externals/build_${pkg}/ || die
	done
}

src_configure() {
	local mycmakeargs=(
		-DGEOIP_BUILTIN=OFF
		-DGOOGLETEST_BUILTIN=OFF
		-DLEVELDB_BUILTIN=OFF
		-DLIBCURL_BUILTIN=OFF
		-DPACPARSER_BUILTIN=OFF
		-DSPARSEHASH_BUILTIN=OFF
		-DSQLITE3_BUILTIN=OFF
		-DTBB_PRIVATE_LIB=OFF
		-DZLIB_BUILTIN=OFF
		-DBUILD_CVMFS=ON
		-DBUILD_LIBCVMFS=ON
		-DINSTALL_MOUNT_SCRIPTS=ON
		$(cmake-utils_use doc BUILD_DOCUMENTATION)
		$(cmake-utils_use server BUILD_SERVER)
	)
	if use test || use test-programs; then
		mycmakeargs+=( -DBUILD_UNITTESTS=ON )
	fi
	use test-programs && mycmakeargs+=( -DINSTALL_UNITTESTS=ON )
	if use debug; then
		mycmakeargs+=(
			$(cmake-utils_use server BUILD_SERVER_DEBUG)
			$(cmake-utils_use test BUILD_UNITTESTS_DEBUG)
		)
	fi
	cmake-utils_src_configure
}

src_install() {
	cmake-utils_src_install
	use doc && dohtml -r doc/html/*
}

pkg_config() {
	einfo "Setting up CernVM-FS client"
	cvmfs_config setup
	einfo "Now edit ${EROOT%/}/etc/cvmfs/default.local"
	einfo "and restart the autofs service"
}
